public class Equation {

    private int a, b;

    public Equation(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public static int getIntFromString(String str) {
        int value;
        try {
            value = Integer.parseInt(str);
        } catch (NumberFormatException exception) {
            value = 0;
        }
        return value;
    }

    public static void main(String args[]) {
        int valeurA = getIntFromString(args.length > 0 ? args[0] : "0");
        int valeurB = getIntFromString(args.length > 1 ? args[1] : "0");
        Equation equa = new Equation(valeurA, valeurB);
        equa.afficher();
        try {
            int x = equa.solution();
            System.out.println("résultat : X = " + x);
        } catch (ArithmeticException exception) {
            System.out.println("Paramètres invalides.");
        }
    }

    public void afficher() {
        System.out.println(a + " * X = " + b);
    }

    int solution() throws ArithmeticException {
        return b / a;
    }

}
