# Capture, levée, propagation et traitement d'une exception

**Objectif :** Utiliser des mécanismes de détection et de gestion des situations d'erreur

Considérer la classe `Equation`. 
Analyser les différentes instructions et identifier celle(s) 
pouvant lever une exception et provoquer l'arrêt de la méthode 
du programme principal.

Améliorer le code précédent de manière à capturer et à traiter les différentes exceptions.
 
On envisagera les trois solutions suivantes :
+ l'exception est capturée et traitée au niveau de l'instruction posant problème,
+ l'exception est propagée à la méthode appelante si elle est levée, et c'est la
méthode appelante qui traite l'exception ;
+ une variante de la méthode solution dans laquelle le test "a=0" est explicitement
réalisé et génère/lève une exception. Celle-ci devra être propagée à la méthode 
appelante qui se chargera d'afficher les informations sur l'exception.