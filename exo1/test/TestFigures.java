import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class TestFigures {

    Figure f;

    @Test
    public void testCercle() {
        f = new Cercle(45.0);
        assertEquals(f.surface(), Math.pow(45.0, 2) * Math.PI);
    }

    @Test
    public void testRectangle() {
        f = new Rectangle(60.0, 2.0);
        assertEquals(f.surface(), 60.0 * 2.0);
    }

    @Test
    public void testCarre() {
        f = new Carre(45.0);
        assertEquals(f.surface(), Math.pow(45.0, 2));
    }

    @Test
    public void testTrapeze() {
        f = new Trapeze(2.0, 45.0, 1.0);
        assertEquals(f.surface(), ((45.0 + 1.0) * 2.0) / 2);
    }
}
