# Classes abstraites et héritage.

**Objectif :** Utiliser l’héritage pour définir de nouvelles classes

La classe `Figure` eprésente de manière abstraite la notion de figure géométrique. 
Elle possède une méthode permettant de fournir la surface d'un objet Figure.

Héritez de `Figure` une classe `Cercle` caractérisée par un rayon, 
une classe `Rectangle`, décrite par une longueur et une largeur, 
une classe `Carre` décrite par la longueur de ses côtés, et 
une classe `Trapeze` définie par une hauteur, une grande base et une petite base.

Donnez des cas de tests dans la classe `TestFigures`, 
vous permettant de valider votre implémentation.
 
