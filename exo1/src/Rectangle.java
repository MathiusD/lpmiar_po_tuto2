public class Rectangle extends Figure {

    private double l;
    private double L;

    public Rectangle(double l, double L) {
        this.l = l;
        this.L = L;
    }

    @Override
    public double surface() {
        return this.L * this.l;
    }
}
