public class Trapeze extends Figure{

    private double h;
    private double B;
    private double b;

    public Trapeze(double h, double B, double b) {
        this.h = h;
        this.B = B > b ? B : b;
        this.b = B > b ? b : B;
    }

    @Override
    public double surface() {
        return ((this.B + this.b) * this.h) / 2;
    }
}
