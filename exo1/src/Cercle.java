public class Cercle extends Figure{

    private double rayon;

    public Cercle(double rayon) {
        this.rayon = rayon;
    }

    @Override
    public double surface() {
        return Math.PI * Math.pow(this.rayon, 2);
    }
}
