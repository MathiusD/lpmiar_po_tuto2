# Les collections 

**Objectif :** Maîtriser la manipulation de collections dynamiques Java

Considérer la classe `Personne`. 

Implémenter une méthode `equals()` permettant de comparer deux personnes : 
on considérera que deux personnes sont identiques si elles ont le même nom et le même prénom 
(sans prendre en compte la casse).

La méthode `toString()` retournera une chaine de caractère concaténant la premiere
 lettre du prénom en majuscule,
suivi d'un point, d'un espace, puis de l'ensemble du nom, avec la première lettre
 du nom uniquement en majuscule.
Exemple, pour `(lanoix,arnaud)`, `toString()` retourne `"A. Lanoix"`.

`TestPersonne` permet de valider votre code.

-------

Considérer la classe `Employe` héritant de Personne. 
Ajoutez le fait que chaque employé possède un numéro qui lui est propre. 

-----
_On désigne une entreprise comme un ensemble d'employés.
Il faut pouvoir ajouter une personne en cas d'embauche et licencier une personne.
Lorsqu'une personne est employée, elle devient un employé, mais elle reste une personne.

Lorsqu'elle quitte l'entreprise, elle redevient une simple personne 
(NB : elle ne devient pas personne comme certains aimeraient nous le faire croire)._

Complétez la classe `Entreprise` reprenant ce comportement : 
pour stocker les employés vous utiliserez une `java.util.List<E>`.
Les méthodes `embaucher()` / `licencier()` retourneront respectivement 
le nouvel employé, la personne employée anciennement .

Les méthodes `employesParOrdreDembauche()`, `employesParOrdreAlphabetiqueNomPrenom()` ou 
 `employesParOrdreAlphabetiquePrenomNom()` renvoient l'ensemble des employés triés selon un certain ordre.
 
 Pour trier des objets en Java, il faut pouvoir les comparer. Deux solutions possibles :
 l'une basée sur l'interface `java.util.Comparable`, l'autre basée sur l'interface `java.util.Comparator`
  NB : regarder les méthodes statiques de la classe java.util.Collections pour le tri.
 
La méthode `anciensEmployes()` renvoie toutes les personnes qui ont été licenciées.