import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestEmploye {

    @Test
    public void testInit() {
        Employe e1 = new Employe("Lanoix", "Arnaud");
        Employe e2 = new Employe("Lanoix", "Arnaud");
        Employe e3 = new Employe("Lanoix", "Arnaud");
        Employe e4 = new Employe("Lanoix", "Arnaud");
        Employe e5 = new Employe("Lanoix", "Arnaud");
        assertEquals(e1.getNb(), 1);
        assertEquals(e2.getNb(), 2);
        assertEquals(e3.getNb(), 3);
        assertEquals(e4.getNb(), 4);
        assertEquals(e5.getNb(), 5);
    }
}
