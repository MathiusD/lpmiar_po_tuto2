import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestEntreprise {

    Entreprise ent;

    Personne[] avengers = {
            new Personne("Iron", "Man"),
            new Personne("Captain", "America"),
            new Personne("Ant", "Man"),
            new Personne("Black", "Widow"),
            new Personne("Scarlet", "Witch"),
            new Personne("Black", "Panther")};

    Personne[] avengers_alphaNom = {
            new Personne("Ant", "Man"),
            new Personne("Black", "Panther"),
            new Personne("Black", "Widow"),
            new Personne("Captain", "America"),
            new Personne("Iron", "Man"),
            new Personne("Scarlet", "Witch")
    };

    Personne[] avengers_alphaPrenom = {
            new Personne("Captain", "America"),
            new Personne("Ant", "Man"),
            new Personne("Iron", "Man"),
            new Personne("Black", "Panther"),
            new Personne("Black", "Widow"),
            new Personne("Scarlet", "Witch")
    };


    @BeforeEach
    public void init() {
        ent = new Entreprise();
    }

    @Test
    public void entrepriseVide() {
        assertEquals(0, ent.nbEmployes());
    }

    @Test
    public void entreprise1embauche() {
        Personne p = new Personne("Stark", "Tony");
        Employe e = ent.embaucher(p);
        assertAll(
                () -> assertNotNull(e),
                () -> assertEquals(1, ent.nbEmployes()),
                () -> assertEquals(p, e),
                () -> assertEquals(e, p)
        );
    }

    @Test
    public void entreprise1embauche1Licenciement() {
        Personne pavant = new Personne("Stark", "Tony");
        Employe e = ent.embaucher(pavant);
        Personne papres = ent.licencier(e);
        assertAll(
                () -> assertNotNull(papres),
                () -> assertEquals(0, ent.nbEmployes()),
                () -> assertEquals(pavant, e),
                () -> assertEquals(e, papres)
        );
    }

    @Test
    public void entreprise1embauche2fois() {
        Personne p = new Personne("Stark", "Tony");
        ent.embaucher(p);
        Employe e = ent.embaucher(p);
        assertAll(
                () -> assertNull(e),
                () -> assertEquals(1, ent.nbEmployes())
        );
    }

    @Test
    public void entreprisePlusieursEmbauches() {
        for (Personne avenger : avengers) {
            ent.embaucher(avenger);
        }
        assertEquals(6, ent.nbEmployes());
    }


    @Test
    public void entreprisePlusieursEmbauches_qqLicenciements() {
        for (Personne avenger : avengers) {
            ent.embaucher(avenger);
        }
        List<Employe> emplois = ent.employesParOrdreDembauche();
        // qq employes à licencier
        Employe e1 = emplois.get(1); //
        Employe e3 = emplois.get(3); // ,
        // licenciements
        Personne p1 = ent.licencier(e1);
        Personne p3 = ent.licencier(e3);
        assertAll(
                () -> assertEquals(4, ent.nbEmployes()),
                () -> assertEquals(new Personne("Captain", "America"), p1),
                () -> assertEquals(new Personne("Black", "Widow"), p3)
        );
    }

    @Test
    public void entrepriseEmployesEmbauche() {
        for (Personne avenger : avengers) {
            ent.embaucher(avenger);
        }
        assertIterableEquals(Arrays.asList(avengers), ent.employesParOrdreDembauche());
    }

    @Test
    public void entrepriseEmployesNomPrenom() {
        for (Personne avenger : avengers) {
            ent.embaucher(avenger);
        }
        assertIterableEquals(Arrays.asList(avengers_alphaNom), ent.employesParOrdreAlphabetiqueNomPrenom());
    }

    @Test
    public void entrepriseEmployesPrenomNom() {
        for (Personne avenger : avengers) {
            ent.embaucher(avenger);
        }
        assertIterableEquals(Arrays.asList(avengers_alphaPrenom), ent.employesParOrdreAlphabetiquePrenomNom());
    }

    @Test
    public void entrepriseTris_pasDeffetDeBord() {
        for (Personne avenger : avengers) {
            ent.embaucher(avenger);
        }
        assertIterableEquals(Arrays.asList(avengers_alphaPrenom), ent.employesParOrdreAlphabetiquePrenomNom());
        assertIterableEquals(Arrays.asList(avengers), ent.employesParOrdreDembauche());
        assertIterableEquals(Arrays.asList(avengers_alphaNom), ent.employesParOrdreAlphabetiqueNomPrenom());
    }

    @Test
    public void entrepriseAnciensEmployes() {
        for (Personne avenger : avengers) {
            ent.embaucher(avenger);
        }
        List<Employe> emplois = ent.employesParOrdreDembauche();
        Personne p1 = ent.licencier(emplois.get(3));
        Personne p2 = ent.licencier(emplois.get(1));
        assertIterableEquals(Arrays.asList(new Personne[]{p1, p2}), ent.anciensEmployes());
    }

    @Test
    public void entrepriseAnciensEmployes_rembauche() {
        for (Personne avenger : avengers) {
            ent.embaucher(avenger);
        }
        List<Employe> emplois = ent.employesParOrdreDembauche();
        Personne p1 = ent.licencier(emplois.get(3));
        Personne p2 = ent.licencier(emplois.get(1));
        ent.embaucher(p2);
        assertIterableEquals(Arrays.asList(new Personne[]{p1}), ent.anciensEmployes());
    }

}
