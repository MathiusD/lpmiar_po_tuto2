import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestPersonne {

    @Test
    public void testEgal() {
        Personne p = new Personne("Lanoix", "Arnaud");
        Assertions.assertAll(
                () -> assertEquals(p, p),
                () -> Assertions.assertSame(p, p)
        );
    }

    @Test
    public void testEgal2() {
        Personne p1 = new Personne("Lanoix", "Arnaud");
        Personne p2 = new Personne("Lanoix", "Arnaud");
        Assertions.assertAll(
                () -> assertEquals(p1, p2),
                () -> assertEquals(p2, p1),
                () -> Assertions.assertNotSame(p1, p2)
        );
    }

    @Test
    public void testEgal3() {
        Personne p1 = new Personne("Lanoix", "Arnaud");
        Personne p2 = new Personne("LANOIX", "ARNAUD");
        Assertions.assertAll(
                () -> assertEquals(p1, p2),
                () -> assertEquals(p2, p1),
                () -> Assertions.assertNotSame(p1, p2)
        );
    }

    @Test
    public void testEgal4() {
        Personne p1 = new Personne("Lanoix", "Arnaud");
        Personne p2 = new Personne("lanoix", "arnaud");
        Assertions.assertAll(
                () -> assertEquals(p1, p2),
                () -> assertEquals(p2, p1),
                () -> Assertions.assertNotSame(p1, p2)
        );
    }

    @Test
    public void testEgal5() {
        Personne p1 = new Personne("Lanoix Brauer", "Arnaud Michel");
        Personne p2 = new Personne("lanoix brauer", "arnaud michel");
        Assertions.assertAll(
                () -> assertEquals(p1, p2),
                () -> assertEquals(p2, p1),
                () -> Assertions.assertNotSame(p1, p2)
        );
    }

    @Test
    public void testDiff() {
        Personne p1 = new Personne("Lanoix", "Muriel");
        Personne p2 = new Personne("lanoix", "arnaud");
        Assertions.assertAll(
                () -> assertNotEquals(p1, p2),
                () -> assertNotEquals(p2, p1),
                () -> Assertions.assertNotSame(p1, p2)
        );
    }

    @Test
    public void testDiff2() {
        Personne p1 = new Personne("arnaud", "lanoix");
        Personne p2 = new Personne("lanoix", "arnaud");
        Assertions.assertAll(
                () -> assertNotEquals(p1, p2),
                () -> assertNotEquals(p2, p1),
                () -> Assertions.assertNotSame(p1, p2)
        );
    }

    @Test
    public void testToString1() {
        Personne p = new Personne("Lanoix", "Arnaud");
        assertEquals("A. Lanoix", p.toString());
    }

    @Test
    public void testToString2() {
        Personne p = new Personne("lanoix", "arnaud");
        assertEquals("A. Lanoix", p.toString());
    }

    @Test
    public void testToString3() {
        Personne p = new Personne("LANOIX", "Arnaud");
        assertEquals("A. Lanoix", p.toString());
    }

}
