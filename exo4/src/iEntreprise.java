import java.util.List;

public interface iEntreprise {
    int nbEmployes();

    Employe embaucher(Personne p);

    Personne licencier(Employe e);

    List<Employe> employesParOrdreDembauche();

    List<Employe> employesParOrdreAlphabetiqueNomPrenom();

    List<Employe> employesParOrdreAlphabetiquePrenomNom();

    List<Personne> anciensEmployes();
}
