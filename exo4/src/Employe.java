public class Employe extends Personne {

    private static int nbEmploye = 0;
    private int nb;

    public int getNb() {
        return this.nb;
    }

    public Employe(String nom, String prenom) {
        super(nom, prenom);
        Employe.nbEmploye += 1;
        this.nb = Employe.nbEmploye;
    }
}
