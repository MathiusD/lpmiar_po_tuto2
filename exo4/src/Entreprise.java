import java.util.*;

public class Entreprise implements iEntreprise {

    private List<Employe> employes;
    private List<Personne> anciens;

    public Entreprise() {
        this.employes = new ArrayList<>();
        this.anciens = new ArrayList<>();
    }

    @Override
    public int nbEmployes() {
        return this.employes.size();
    }


    @Override
    public Employe embaucher(Personne p) {
        if (this.employes.contains(p))
            return null;
        if (this.anciens.contains(p))
            this.anciens.remove(p);
        Employe e = new Employe(p.getNom(), p.getPrenom());
        this.employes.add(e);
        return e;
    }

    @Override
    public Personne licencier(Employe e) {
        if (this.employes.contains(e))
            this.anciens.add(e);
        return this.employes.remove(e) ? e : null;
    }

    @Override
    public List<Employe> employesParOrdreDembauche() {
        List<Employe> list = this.employes;
        list.sort(Comparator.comparingInt(Employe::getNb));
        return list;
    }

    @Override
    public List<Employe> employesParOrdreAlphabetiqueNomPrenom() {
        List<Employe> list = this.employes;
        list.sort((o1, o2) -> {
            int value = o1.getNom().compareToIgnoreCase(o2.getNom());
            if (value == 0)
                return o1.getPrenom().compareToIgnoreCase(o2.getPrenom());
            else
                return value;
        });
        return list;
    }

    @Override
    public List<Employe> employesParOrdreAlphabetiquePrenomNom() {
        List<Employe> list = this.employes;
        list.sort((o1, o2) -> {
            int value = o1.getPrenom().compareToIgnoreCase(o2.getPrenom());
            if (value == 0)
                return o1.getNom().compareToIgnoreCase(o2.getNom());
            else
                return value;
        });
        return list;
    }

    @Override
    public List<Personne> anciensEmployes() {
        return this.anciens;
    }


}
