# Interfaces en Java

**Objectif :** Définir dans une classe de base des fonctionnalités communes à toutes ses descendantes.

Définir une classe `Moteur`, avec au moins deux attributs :
+ le premier reflète le niveau de carburant actuel
+ et le second la consommation moyenne pour 100 km. 
 
Munir cette classe de 5 méthodes : 

+ `niveauCarburant()` - renvoie le niveau actuel du carburant,
+ `ajouterCarburant()` - prend en argument la quantité de carburant à ajouter,
+ `demarrer()` - démarrer le moteur consomme 0.2 L de carburant et n'est possible 
                 que si le niveau de carburant est supérieur  à 0.5 L,
+ `arreter()`  - si le moteur est démarré,
+ `utiliser(int nbkm)` - met à jour l'estimation du niveau de carburant en fonction du
 nombre de kilomètres parcourus, si le moteur est démarré. Attention : si le trajet 'nest pas possible, 
 alors il n'a pas lieu.
 
De plus, chaque méthode devra afficher, dans la console, 
l'action effectuée avec le niveau de carburant actuel et/ou la consommation.

La classe de test `TestMoteur ` vous permet de valider votre développement.

---

La classe abstraite `Vehicule` comporte un attribut correspondant au modèle
du véhicule et deux méthodes abstraites : `demarrer() `et `arreter()`qui retourneront
une valeur booléenne indiquant si l'opération est possible ou non.

Ecrire une classe `VehiculeMotorise` 
héritant de `Vehicule` et possédant un attribut de type `Moteur`. 
Redéfinir les méthodes `demarrer()` et `arreter()`.
Ajouter une méthode `remplirReservoir()` 
prenant en argument la quantité de carburant à ajouter.



---
Considérer l'interface `VehiculeRoulant` qui défini une méthode `rouler()` 
prenant en argument le nombre de kilomètres parcourus.
Créer deux sous-classes de `VehiculeMotorise` : `Voiture` et `Moto` qui implémente l'interface `VehiculeRoulant`. 
Donner des cas de tests dans `TestVoitureMoto` permettant de valider les classes `Voiture` et `Moto`.


---
Créer une classe `Velo` héritant de `Vehicule` et qui implémente l'interface `VehiculeRoulant`. 
NB : un vélo démarre, s'arrête et roule toujours.

---
Considérer l'interface `DeuxRoues` 
permettant de changer le pneu avant 
ou le pneu arrière par les méthodes `changerPneuAvant()` et `changerPneuArriere()`. 
Appliquer cette interface aux classes `Velo `et `Moto`.
Afficher simplement un message dans la console pour le traitement de ces deux méthodes.

---
Considérer l'interface `VehiculeFlottant`.
Créer une interface `VehiculeAmphibie` héritant des interfaces `VehiculeRoulant `et `VehiculeFlottant`.
Créer une sous-classe de `VehiculeMotorise` : Barge implémentant l'interface `VehiculeAmphibie`.

