public class Velo extends Vehicule implements VehiculeRoulant, DeuxRoues{

    final protected static String TAG = "Velo";
    private boolean launched;

    public Velo(String modele) {
        super(modele);
        this.launched = false;
    }

    @Override
    public boolean demarrer() {
        if (!this.launched) {
            this.launched = true;
            return true;
        }
        return false;
    }

    @Override
    public boolean arreter() {
        if (this.launched) {
            this.launched = false;
            return true;
        }
        return false;
    }

    @Override
    public boolean rouler(int nbkm) {
        return true;
    }

    @Override
    public void changerPneuAvant() {
        Velo.consoleLog("Pneu avant changé !");
    }

    @Override
    public void changerPneuArriere() {
        Velo.consoleLog("Pneu arrière changé !");
    }
}
