public class Barge extends VehiculeMotorise implements VehiculeAmphibie{

    public Barge(String modele, Moteur moteur) {
        super(modele, moteur);
    }

    @Override
    public boolean naviguer(int nbkm) {
        return this.engine.utiliser(nbkm);
    }

    @Override
    public boolean rouler(int nbkm) {
        return this.engine.utiliser(nbkm);
    }
}
