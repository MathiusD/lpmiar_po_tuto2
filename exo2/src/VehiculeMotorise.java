public class VehiculeMotorise extends Vehicule {

    protected Moteur engine;

    public VehiculeMotorise(String modele, Moteur moteur) {
        super(modele);
        this.engine = moteur;
    }

    @Override
    public boolean demarrer() {
        return this.engine.demarrer();
    }

    @Override
    public boolean arreter() {
        return this.engine.arreter();
    }

    public void remplirReservoir(double carburantEnPlus) {
        this.engine.ajouterCarburant(carburantEnPlus);
    }


}
