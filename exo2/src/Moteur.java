public class Moteur extends Log{

    final protected static String TAG = "Moteur";
    private double carburant;
    private double consommation;
    private boolean launched;

    public Moteur(double carburant, double consommmation) {
        this.carburant = carburant;
        this.consommation = consommmation / 100;
        this.launched = false;
    }
    public double niveauCarburant() {
        Moteur.consoleLog(String.format("Show carburant lvl (%f).", this.carburant));
        return this.carburant;
    }

    public void ajouterCarburant(double carburantEnPlus) {
        Moteur.consoleLog(String.format("Add carburant (%f) carburant is now : %f.", carburantEnPlus, this.carburant));
        this.carburant += carburantEnPlus;
    }

    public boolean demarrer() {
        Moteur.consoleLog("Attempt Started Moteur.");
        if (this.carburant >= 0.5) {
            if (!this.launched) {
                this.carburant -= 0.2;
                Moteur.consoleLog(String.format("Start Moteur (Carburant is now : %f).", this.carburant));
                this.launched = true;
                return true;
            } else
                Moteur.consoleLog("Moteur is already started.");
        } else
            Moteur.consoleLog(String.format("Moteur doesn't have enought carburant (%f).", this.carburant));
        return false;
    }

    public boolean arreter() {
        Moteur.consoleLog("Attempt Stopped Moteur.");
        if (this.launched) {
            Moteur.consoleLog("Stop Moteur.");
            this.launched = false;
            return true;
        }
        Moteur.consoleLog("Moteur isn't Launched.");
        return false;
    }

    public boolean utiliser(int nbkm) {
        Moteur.consoleLog(String.format("Attempt ride in %d kms.", nbkm));
        if (this.launched) {
            double nbCarbu = nbkm * this.consommation;
            if (nbCarbu <= this.carburant) {
                this.carburant -= nbCarbu;
                Moteur.consoleLog(String.format("Ride in %d kms (Carburant is now %f).", nbkm, this.carburant));
                return true;
            }
            Moteur.consoleLog(String.format("Ride isn't possible (Carburant required (%f) is more than carburant in Moteur (%f)", nbCarbu, this.carburant));
        } else
            Moteur.consoleLog("Moteur isn't launched.");
        return false;
    }


}
