// OK

public abstract class Vehicule extends Log{

    private String modele;

    public Vehicule(String modele) {
        this.modele = modele;
    }

    public abstract boolean demarrer();

    public abstract boolean arreter();
}
