public class Moto extends VehiculeMotorise implements DeuxRoues, VehiculeRoulant{

    final protected static String TAG = "Moto";

    @Override
    public boolean rouler(int nbkm) {
        return this.engine.utiliser(nbkm);
    }

    public Moto(String modele, Moteur moteur) {
        super(modele, moteur);
    }

    @Override
    public void changerPneuAvant() {
        Velo.consoleLog("Pneu avant changé !");
    }

    @Override
    public void changerPneuArriere() {
        Velo.consoleLog("Pneu arrière changé !");
    }
}
