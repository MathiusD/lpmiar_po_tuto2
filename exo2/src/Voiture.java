public class Voiture extends VehiculeMotorise implements VehiculeRoulant{

    public Voiture(String modele, Moteur moteur) {
        super(modele, moteur);
    }

    @Override
    public boolean rouler(int nbkm) {
        return this.engine.utiliser(nbkm);
    }
}
