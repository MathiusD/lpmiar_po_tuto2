import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestMoto {

    @Test
    public void testMoto() {
        Moto v1 = new Moto("Brup", new Moteur(40, 10));
        v1.demarrer();
        assertTrue(v1.rouler(45));
        assertFalse(v1.rouler(99999));
        v1.arreter();
        assertFalse(v1.rouler(1));
    }
}
