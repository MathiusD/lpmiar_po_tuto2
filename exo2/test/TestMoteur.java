import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestMoteur {

    Moteur m;

    @Test
    public void moteur_init() {
        m = new Moteur(40, 10);
        assertEquals(40, m.niveauCarburant(), 0.001);
    }

    @Test
    public void moteur_init2() {
        m = new Moteur(0.5, 10);
        assertEquals(0.5, m.niveauCarburant(), 0.001);
    }

    @Test
    public void moteur_demarrer() {
        m = new Moteur(40, 10);
        assertTrue(m.demarrer());
        assertEquals(39.8, m.niveauCarburant(), 0.001);
    }

    @Test
    public void moteur_demarrer3() {
        m = new Moteur(0.5, 10);
        assertTrue(m.demarrer());
        assertEquals(0.3, m.niveauCarburant(), 0.001);
    }

    @Test
    public void moteur_demarrer_non() {
        m = new Moteur(0.4, 10);
        assertFalse(m.demarrer());
    }

    @Test
    public void moteur_arreter_non() {
        m = new Moteur(0.4, 10);
        assertFalse(m.arreter());
    }

    @Test
    public void moteur_arreter_non2() {
        m = new Moteur(0.4, 10);
        m.demarrer();
        assertFalse(m.arreter());
    }

    @Test
    public void moteur_arreter() {
        m = new Moteur(40.0, 10);
        m.demarrer(); // 39.8
        assertTrue(m.arreter());
    }

    @Test
    public void moteur_arreter_demarrer() {
        m = new Moteur(40.0, 10);
        m.demarrer(); // 39.8
        m.arreter();
        assertTrue(m.demarrer()); // 39.6
        assertEquals(39.6, m.niveauCarburant(), 0.001);
    }

    @Test
    public void moteur_utiliser() {
        m = new Moteur(40.0, 10);
        m.demarrer(); // 39.8
        assertTrue(m.utiliser(100));
        assertEquals(29.8, m.niveauCarburant(), 0.001);
    }

    @Test
    public void moteur_utiliser2() {
        m = new Moteur(40.0, 10);
        m.demarrer(); // 39.8
        assertTrue(m.utiliser(10));
        assertEquals(38.8, m.niveauCarburant(), 0.001);
    }

    @Test
    public void moteur_utiliser3() {
        m = new Moteur(40.0, 10);
        m.demarrer(); // 39.8
        assertTrue(m.utiliser(1));
        assertEquals(39.7, m.niveauCarburant(), 0.001);
    }

    @Test
    public void moteur_utiliser4() {
        m = new Moteur(40.0, 10);
        m.demarrer(); // 39.8
        assertTrue(m.utiliser(300));
        assertEquals(9.8, m.niveauCarburant(), 0.001);
    }

    @Test
    public void moteur_utiliser_non() {
        m = new Moteur(40.0, 10);
        m.demarrer(); // 39.8
        assertFalse(m.utiliser(1000));
        assertEquals(39.8, m.niveauCarburant(), 0.001);
    }


}
