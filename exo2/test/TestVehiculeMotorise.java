import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestVehiculeMotorise {

    @Test
    public void TestVehiculeMotorise() {
        Vehicule m1 = new VehiculeMotorise("Brup", new Moteur(40, 10));
        assertTrue(m1.demarrer());
        assertFalse(m1.demarrer());
        assertTrue(m1.arreter());
        assertFalse(m1.arreter());
    }
}
