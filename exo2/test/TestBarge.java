import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class TestBarge {
    // TODO

    @Test
    public void testBarge() {
        Barge b1 = new Barge("Blap", new Moteur(40, 10));
        b1.demarrer();
        assertTrue(b1.rouler(22));
        assertTrue(b1.naviguer(22));
        assertFalse(b1.rouler(99999));
        assertFalse(b1.naviguer(99999));
        b1.arreter();
        assertFalse(b1.rouler(1));
        assertFalse(b1.naviguer(1));
    }
}
