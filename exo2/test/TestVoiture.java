import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestVoiture {


    @Test
    public void testVoiture() {
        Voiture v1 = new Voiture("Brup", new Moteur(40, 10));
        v1.demarrer();
        assertTrue(v1.rouler(45));
        assertFalse(v1.rouler(99999));
        v1.arreter();
        assertFalse(v1.rouler(1));
    }
}