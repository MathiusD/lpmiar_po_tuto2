import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestVelo {

    @Test
    public void testVelo() {
        Velo v1 = new Velo("Blap");
        assertTrue(v1.rouler(2));
        assertTrue(v1.demarrer());
        assertFalse(v1.demarrer());
        assertTrue(v1.rouler(999999));
        assertTrue(v1.arreter());
        assertFalse(v1.arreter());
        assertTrue(v1.rouler(1));
    }
}
