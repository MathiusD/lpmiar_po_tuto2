import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestBocal {

    Bocal bocal;

    @BeforeEach
    public void init() {
        bocal = new Bocal();
    }

    @Test
    public void bocalVide() {
        assertEquals(0, bocal.compter());
    }

    @Test
    public void bocal1Bonbon() {

        assertTrue(bocal.ajouter(new Bonbon()));
        assertEquals(1, bocal.compter());
    }

    @Test
    public void bocal2Bonbons() {
        assertTrue(bocal.ajouter(new Bonbon()));
        assertTrue(bocal.ajouter(new Bonbon()));
        assertEquals(2, bocal.compter());
    }

    @Test
    public void bocal2Bonbons_ajoutDeja() {
        Bonbon b = new Bonbon();
        assertTrue(bocal.ajouter(b));
        assertTrue(bocal.ajouter(new Bonbon()));
        assertFalse(bocal.ajouter(b));
        assertEquals(2, bocal.compter());
    }


    @Test
    public void bocal3Bonbons_del2() {
        Bonbon b = new Bonbon();
        bocal.ajouter(new Bonbon());
        bocal.ajouter(b);
        bocal.ajouter(new Bonbon());
        assertTrue(bocal.enlever(b));
        assertEquals(2, bocal.compter());
    }

    @Test
    public void bocal3Bonbons_del1() {
        Bonbon b = new Bonbon();
        bocal.ajouter(b);
        bocal.ajouter(new Bonbon());
        bocal.ajouter(new Bonbon());
        assertTrue(bocal.enlever(b));
        assertEquals(2, bocal.compter());
    }

    @Test
    public void bocal3Bonbons_del3() {
        Bonbon b = new Bonbon();
        bocal.ajouter(new Bonbon());
        bocal.ajouter(new Bonbon());
        bocal.ajouter(b);
        assertTrue(bocal.enlever(b));
        assertEquals(2, bocal.compter());
    }

    @Test
    public void bocal3Bonbons_del_pasLa() {
        Bonbon b = new Bonbon();
        bocal.ajouter(new Bonbon());
        bocal.ajouter(new Bonbon());
        bocal.ajouter(new Bonbon());
        assertFalse(bocal.enlever(b));
        assertEquals(3, bocal.compter());
    }

    @Test
    public void bocalRemplir0() {
        bocal.remplir(0);
        assertEquals(0, bocal.compter());
    }

    @Test
    public void bocalRemplir3() {
        bocal.remplir(3);
        assertEquals(3, bocal.compter());
    }

    @Test
    public void bocalRemplir10000() {
        bocal.remplir(10000);
        assertEquals(10000, bocal.compter());
    }

    @Test
    public void bocalRemplir3_vider() {
        bocal.remplir(3);
        bocal.vider();
        assertEquals(0, bocal.compter());
    }

    @Test
    public void bocalRemplir10000_vider() {
        bocal.remplir(10000);
        bocal.vider();
        assertEquals(0, bocal.compter());
    }

    @Test
    public void bocalViderDansLordre() {
        List<Bonbon> bonbons = Arrays.asList(new Bonbon[]{
                new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon()});
        for (Bonbon b : bonbons) {
            bocal.ajouter(b);
        }
        Assertions.assertIterableEquals(bonbons, bocal.vider());
        assertEquals(0, bocal.compter());
    }

    @Test
    public void bocalViderDansLordreAttention() {
        List<Bonbon> bonbons = new LinkedList(Arrays.asList(new Bonbon[]{
                new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon()}));
        for (Bonbon b : bonbons) {
            bocal.ajouter(b);
        }
        Bonbon b = new Bonbon();
        bocal.ajouter(b);
        bonbons.add(b);
        //
        Assertions.assertIterableEquals(bonbons, bocal.vider());
        assertEquals(0, bocal.compter());
    }

    @Test
    public void bocalTransvaser() {
        List<Bonbon> bonbons = new LinkedList(Arrays.asList(new Bonbon[]{
                new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon()}));
        for (Bonbon b : bonbons) {
            bocal.ajouter(b);
        }
        Bocal nouveau = new Bocal();
        nouveau.transvaser(bocal);
        Assertions.assertAll(
                () -> assertEquals(7, nouveau.compter()),
                () -> assertEquals(0, bocal.compter())
        );
        Assertions.assertIterableEquals(bonbons, nouveau.vider());
    }

    @Test
    public void bocalTransvaser_nonVide() {
        List<Bonbon> bonbons = new LinkedList(Arrays.asList(new Bonbon[]{
                new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon()}));
        for (Bonbon b : bonbons) {
            bocal.ajouter(b);
        }
        Bocal nouveau = new Bocal();
        Bonbon b1 = new Bonbon();
        nouveau.ajouter(b1);
        bonbons.add(b1);
        Bonbon b2 = new Bonbon();
        nouveau.ajouter(b2);
        bonbons.add(b2);
        Bonbon b3 = new Bonbon();
        nouveau.ajouter(b3);
        bonbons.add(b3);
        nouveau.transvaser(bocal);
        Assertions.assertAll(
                () -> assertEquals(10, nouveau.compter()),
                () -> assertEquals(0, bocal.compter())
        );
        Assertions.assertIterableEquals(bonbons, nouveau.vider());
    }

    @Test
    public void bocalTransvaser_nonVide3() {
        List<Bonbon> bonbons = new LinkedList(Arrays.asList(new Bonbon[]{
                new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon()}));
        for (Bonbon b : bonbons) {
            bocal.ajouter(b);
        }
        Bocal nouveau = new Bocal();
        List<Bonbon> bonbons2 = new LinkedList(Arrays.asList(new Bonbon[]{
                new Bonbon(), new Bonbon(), new Bonbon()}));
        for (Bonbon b : bonbons2) {
            bocal.ajouter(b);
        }
        bocal.transvaser(nouveau);
        bonbons.addAll(bonbons2);
        Assertions.assertAll(
                () -> assertEquals(10, bocal.compter()),
                () -> assertEquals(0, nouveau.compter())
        );
        Assertions.assertIterableEquals(bonbons, bocal.vider());
    }


    @Test
    public void bocalDiviser() {
        List<Bonbon> bonbons = new LinkedList(Arrays.asList(new Bonbon[]{
                new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon()}));
        for (Bonbon b : bonbons) {
            bocal.ajouter(b);
        }
        Bocal bocal1 = new Bocal();
        Bocal bocal2 = new Bocal();
        bocal.diviser(bocal1, bocal2);
        Assertions.assertAll(
                () -> assertEquals(0, bocal.compter()),
                () -> assertEquals(3, bocal1.compter()),
                () -> assertEquals(3, bocal2.compter())
        );
    }

    @Test
    public void bocalDiviser2() {
        List<Bonbon> bonbons = new LinkedList(Arrays.asList(new Bonbon[]{
                new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon()}));
        for (Bonbon b : bonbons) {
            bocal.ajouter(b);
        }
        Bocal bocal1 = new Bocal();
        Bocal bocal2 = new Bocal();
        bocal.diviser(bocal1, bocal2);
        Assertions.assertAll(
                () -> assertEquals(0, bocal.compter()),
                () -> assertEquals(4, bocal1.compter()),
                () -> assertEquals(3, bocal2.compter())
        );
    }

    @Test
    public void bocalDiviser_nonVide() {
        List<Bonbon> bonbons = new LinkedList(Arrays.asList(new Bonbon[]{
                new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon(), new Bonbon()}));
        for (Bonbon b : bonbons) {
            bocal.ajouter(b);
        }
        Bocal bocal1 = new Bocal();
        bocal1.ajouter(new Bonbon());
        bocal1.ajouter(new Bonbon());
        Bocal bocal2 = new Bocal();
        bocal2.ajouter(new Bonbon());
        bocal.diviser(bocal1, bocal2);
        Assertions.assertAll(
                () -> assertEquals(0, bocal.compter()),
                () -> assertEquals(6, bocal1.compter()),
                () -> assertEquals(4, bocal2.compter())
        );
    }

    @Test
    public void bocalDiviserVide() {
        Bocal bocal1 = new Bocal();
        bocal1.ajouter(new Bonbon());
        bocal1.ajouter(new Bonbon());
        Bocal bocal2 = new Bocal();
        bocal2.ajouter(new Bonbon());
        bocal.diviser(bocal1, bocal2);
        Assertions.assertAll(
                () -> assertEquals(0, bocal.compter()),
                () -> assertEquals(2, bocal1.compter()),
                () -> assertEquals(1, bocal2.compter())
        );
    }

    @Test
    public void bocalDiviserPlein() {
        bocal.remplir(100000);
        Bocal bocal1 = new Bocal();
        bocal1.ajouter(new Bonbon());
        bocal1.ajouter(new Bonbon());
        Bocal bocal2 = new Bocal();
        bocal2.ajouter(new Bonbon());
        bocal.diviser(bocal1, bocal2);
        Assertions.assertAll(
                () -> assertEquals(0, bocal.compter()),
                () -> assertEquals(50002, bocal1.compter()),
                () -> assertEquals(50001, bocal2.compter())
        );
    }

    @Test
    public void testVider_100_0() throws PasAssezDeBonbonsException {
        bocal.remplir(100);
        List<Bonbon> sac = bocal.enlever(0);
        Assertions.assertAll(
                () -> assertEquals(100, bocal.compter()),
                () -> Assertions.assertEquals(0, sac.size())
        );
    }

    @Test
    public void testVider_100_50() throws PasAssezDeBonbonsException {
        bocal.remplir(100);
        List<Bonbon> sac = bocal.enlever(50);
        Assertions.assertAll(
                () -> assertEquals(50, bocal.compter()),
                () -> Assertions.assertEquals(50, sac.size())
        );
    }

    @Test
    public void testVider_100_100() throws PasAssezDeBonbonsException {
        bocal.remplir(100);
        List<Bonbon> sac = bocal.enlever(100);
        Assertions.assertAll(
                () -> assertEquals(0, bocal.compter()),
                () -> Assertions.assertEquals(100, sac.size())
        );
    }

    @Test
    public void testVider_0_0() throws PasAssezDeBonbonsException {
        //bocal.remplir(100);
        List<Bonbon> sac = bocal.enlever(0);
        Assertions.assertAll(
                () -> assertEquals(0, bocal.compter()),
                () -> Assertions.assertEquals(0, sac.size())
        );
    }

    @Test
    public void testVider_10_11() {
        bocal.remplir(10);
        Assertions.assertThrows(PasAssezDeBonbonsException.class,
                () -> bocal.enlever(11));
        assertEquals(10, bocal.compter());
    }

    @Test
    public void testVider_0_11() {
        Assertions.assertThrows(PasAssezDeBonbonsException.class,
                () -> bocal.enlever(11));
        assertEquals(0, bocal.compter());
    }


    @Test
    public void bocalIterator() {
        List<Bonbon> sac = new LinkedList<>();
        for (int i = 0; i < 100000; i++) {
            Bonbon bonbon = new Bonbon();
            sac.add(bonbon);
            bocal.ajouter(bonbon);
        }
        Assertions.assertIterableEquals(sac, bocal);
    }


}
