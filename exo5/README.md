# Les collections 

**Objectif :** Maîtriser la manipulation de collections dynamiques Java

Considérez la classe `Bonbon`.

On va définir une classe `Bocal` qui contiendra des bonbons. 
NB : un bonbon ne pourra être qu'une fois dans le bocal, c'est-à-dire que les bonbons seront stockés 
dans un `HashSet<E>`.

La classe `Bocal` implémentera toutes les méthodes indiquées par l'interface `iBocal`.

