import java.util.Objects;

public class Bonbon implements Comparable<Bonbon> {

    private static int num = 1;
    private int n;

    public Bonbon() {
        this.n = num++;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bonbon)) return false;
        Bonbon bonbon = (Bonbon) o;
        return n == bonbon.n;
    }

    @Override
    public int hashCode() {
        return Objects.hash(n) * 31;
    }

    @Override
    public int compareTo(Bonbon o) {
        return Integer.compare(this.n, o.n);
    }
}
