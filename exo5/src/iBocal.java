import java.util.Iterator;
import java.util.List;

public interface iBocal extends Iterable<Bonbon> {

    /**
     * ajoute le bonbon dans le bocal
     *
     * @param b le bonbon à ajouter
     * @return true si le bonbon est bien ajouté
     */
    boolean ajouter(Bonbon b);

    /**
     * @return le nombre de bonbons contenus dans le bocal
     */
    int compter();

    /**
     * enlever du bocal le bonbon indiqué
     *
     * @param b le bonbon à enlever
     * @return true si le bonbon est bien enlevé
     */
    boolean enlever(Bonbon b);

    /**
     * remplir le bocal par des bonbons quelconques
     *
     * @param nb le nombre de bonbons
     */
    void remplir(int nb);

    /**
     * vide le bocal et liste tous les bonbons
     *
     * @return les bonbons qui étaient contenus par le bocal, dans l'ordre où les bonbons ont été créés
     */
    List<Bonbon> vider();


    /**
     * tranvase tous les bonbons du bocal passé en paramètre dans le bocal courant
     *
     * @param bocal le bocal à vider
     */
    void transvaser(iBocal bocal);

    /**
     * divise le contenu du bocal courant dans les deux bocaux passés en paramètre,
     * en commençant par le bocal de gauche
     *
     * @param gauche le 1er bocal à remplir
     * @param droit  le 2ième bocal à remplir
     */
    void diviser(iBocal gauche, iBocal droit);

    /**
     * enlever du bocal le nombre de bonbons indiqués
     *
     * @param nb le nombre de bonbons
     * @return les bonbons enlevés du bocal
     * @throws PasAssezDeBonbonsException s'il n'y a pas assez de bonbons dans le bocal
     */
    List<Bonbon> enlever(int nb) throws PasAssezDeBonbonsException;


    /**
     * renvoie un iterateur peremettant de parcourir les bonbons contenu dans le bocal,
     * dans l'ordre où les bonbons ont été créés
     *
     * @return
     */
    @Override
    Iterator<Bonbon> iterator();
}
