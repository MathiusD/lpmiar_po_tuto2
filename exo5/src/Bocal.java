import java.util.*;

public class Bocal implements iBocal {

    HashSet<Bonbon> set;

    public Bocal() {
        this.set = new HashSet<>();
    }

    @Override
    public boolean ajouter(Bonbon b) {
        return this.set.add(b);
    }

    @Override
    public int compter() {
        return this.set.size();
    }

    @Override
    public void remplir(int nb) {
        for (int compt = 0; compt < nb;)
            compt += this.ajouter(new Bonbon()) ? 1 : 0;
    }

    private List<Bonbon> setToList() {
        List<Bonbon> list = new ArrayList<>();
        list.addAll(this.set);
        list.sort(Bonbon::compareTo);
        return list;
    }

    @Override
    public List<Bonbon> vider() {
        List<Bonbon> list = this.setToList();
        this.set.clear();
        return list;
    }

    @Override
    public boolean enlever(Bonbon b) {
        if (this.set.contains(b)) {
            this.set.remove(b);
            return true;
        }
        return false;
    }

    @Override
    public void transvaser(iBocal bocal) {
        for (Bonbon b: bocal.vider())
            this.ajouter(b);
    }

    @Override
    public void diviser(iBocal gauche, iBocal droit) {
        int nb = this.compter();
        int nbMoy = nb / 2;
        int nbGauche = nb % 2 == 1 ? nbMoy + 1 : nbMoy;
        try {
            for (Bonbon b : this.enlever(nbGauche))
                gauche.ajouter(b);
            for (Bonbon b : this.enlever(nbMoy))
                droit.ajouter(b);
        } catch (PasAssezDeBonbonsException exception) {

        }
    }

    @Override
    public List<Bonbon> enlever(int nb) throws PasAssezDeBonbonsException {
        int compt = nb;
        List<Bonbon> list = new ArrayList<>();
        for (Bonbon b: this.setToList()) {
            if (compt == 0 || this.compter() == 0)
                break;
            list.add(b);
            compt -= 1;
        }
        if (compt != 0)
            throw new PasAssezDeBonbonsException(String.format("Bocal vide, on ne peut pas enlever encore %d Bombons.", compt));
        else
            for (Bonbon b: list)
                this.enlever(b);
            return list;
    }

    @Override
    public Iterator<Bonbon> iterator() {
        return this.setToList().iterator();
    }
}
