public class PasAssezDeBonbonsException extends Exception {

    public PasAssezDeBonbonsException(String msg) {
        super(msg);
    }
}
