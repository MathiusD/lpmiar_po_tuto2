import java.util.*;

public class RepertoireTel implements iRepertoireTel {

    private Map<Personne, List<NumeroTel>> annuaire;

    public RepertoireTel() {
        this.annuaire = new LinkedHashMap<>();
    }

    @Override
    public void ajouterContact(Personne nouveauContact, NumeroTel numero) throws ContactDejaPresentException{
        if (!this.annuaire.containsKey(nouveauContact)) {
            List<NumeroTel> numeros = new ArrayList<>();
            numeros.add(numero);
            this.annuaire.put(nouveauContact, numeros);
        } else
            throw new ContactDejaPresentException(String.format("Personne %s is already in RepertoireTel", nouveauContact));
    }

    @Override
    public void ajouterNumero(Personne contactExistant, NumeroTel numero)
            throws ContactInconnuException, NumeroDejaPresentException {
        if (this.annuaire.containsKey(contactExistant))
            if (!this.annuaire.get(contactExistant).contains(numero))
                this.annuaire.get(contactExistant).add(numero);
            else
                throw new NumeroDejaPresentException(String.format("Numero %s is already in RepertoireTel", numero));
        else
            throw new ContactInconnuException(String.format("Personne %s isn't in RepertoireTel", contactExistant));
    }

    @Override
    public List<Personne> listerContacts() {
        return new ArrayList<>(this.annuaire.keySet());
    }

    @Override
    public List<NumeroTel> listerNumeros(Personne contactExistant) throws ContactInconnuException {
        if (!this.annuaire.containsKey(contactExistant))
            throw new ContactInconnuException(String.format("Personne %s isn't in RepertoireTel", contactExistant));
        return new ArrayList<>(this.annuaire.get(contactExistant));
    }

    @Override
    public void supprimerNumero(Personne contactExistant, NumeroTel numero) throws ContactInconnuException, NumeroInconnuException{
        if (this.annuaire.containsKey(contactExistant))
            if (this.annuaire.get(contactExistant).contains(numero)) {
                this.annuaire.get(contactExistant).remove(numero);
                if (this.annuaire.get(contactExistant).size() == 0)
                    this.annuaire.remove(contactExistant);
            }
            else
                throw new NumeroInconnuException(String.format("Numero %s is already in RepertoireTel", numero));
        else
            throw new ContactInconnuException(String.format("Personne %s isn't in RepertoireTel", contactExistant));
    }
}
