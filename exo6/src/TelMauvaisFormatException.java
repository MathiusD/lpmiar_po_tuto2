public class TelMauvaisFormatException extends Exception {

    public TelMauvaisFormatException(String msg) {
        super(msg);
    }
}
