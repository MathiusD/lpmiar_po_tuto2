public class NumeroDejaPresentException extends Exception {
    public NumeroDejaPresentException(String s) {
        super(s);
    }
}
