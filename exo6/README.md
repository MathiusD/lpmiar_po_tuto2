# Les collections 

**Objectif :** Maîtriser la manipulation de collections dynamiques Java, en particulier les Maps

Considérez la classe `Personne` de l'exercice 4.

Complétez la classe `NumeroTel` qui stocke un numero de téléphone correctement formaté.

------
Complétez la classe `RepertoireTel`. Cette classe associe à une `Personne` 
un ou plusieurs `NumeroTel`. 
On pourra donc utiliser une structure de type `Map` pour stocker ces données.

On devra disposer des méthodes suivantes :

+ `ajouterContact(nouveauContact, numero)`
+ `ajouterNumero(contactExistant, numero)`
+ `listerContacts()`
+ `listerNumeros(contactExistant)`
+ `supprimerNumero(contactExistant, numero)`

NB : lorsqu'une personne n'a plus de numéro, elle est supprimée du répertoire. 